/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package com.mycompany.mavenproject2;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author wando
 */
public class Mavenproject2 {
    
    private static List<Valor> lista = new ArrayList<>();
    
    public static void main(String[] args) {
        int numeroInserido = 0;

        Valor valor;

        Scanner in =  new Scanner(System.in);
        System.out.print("Informe o numero: ");
        numeroInserido = in.nextInt();
        
         
        for(int c=1;c<=numeroInserido;c++){
            for(int i = 1; i<= numeroInserido;i++){
            
            for(int j = 1; j<= numeroInserido; j++){
                
                if(i+j == c){
                valor= new Valor(i,j); 
                
                
                if(!verificar(valor)) {
                    lista.add(valor);
                }
                
                }
            }
            
        }
            System.out.println("Numero: "+ c + "\nSomas possiveis:");
            System.out.println(lista);
            lista.clear();
        }
        
        
    }
    
    private static boolean verificar(Valor valor){
        Valor valorInv = new Valor(valor.getB(),valor.getA());
            
            for(Valor listaValor : lista){
                if(listaValor.getA() == valorInv.getA() && listaValor.getB() == valorInv.getB() ){
                       
                        return true;
                }
                 
            }  
            
            return false;
        
     
    }
}

